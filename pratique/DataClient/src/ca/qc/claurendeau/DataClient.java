package ca.qc.claurendeau;

import ca.qc.claurendeau.exceptions.DatabaseException;

public class DataClient {
    private DataServer dataServer;

    public static void main(String[] args) throws DatabaseException {
        DataServer ds = new DataServer();
        DataClient dataClient = new DataClient(ds);
        for (int i = 0; i < 10; i++) {
            dataClient.processData(i);
        }
    }

    public DataClient(DataServer dataServer) {
        this.dataServer = dataServer;
    }

    public int processData(int entryID) throws DatabaseException {
        dataServer.initiateNewTransaction(entryID);

        // Remplacez le pseudo-code par votre code

        // Implémentez la gestion des erreurs
        // - Exceptions
        // - Données invalides
        // - Pas de données reçues

        // initiateNewTransaction
        // getCSVData
        // extraction des données à partir de getCSVData
        // calcul de output (output = inputLeft + inputRight)
        // si okToSendVoltage(output, coefficient)
        // sendVoltage(output, coefficient)
        // finalizeTransaction

        // retourne le voltage envoyé (formule : output * coefficient)
        // retourne -1 si aucun voltage envoyé

        return -1;
    }
}
