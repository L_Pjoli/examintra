
420-504 -- EXAMEN INTRA -- Automne 2018
---------------------------------------

PROCÉDURE

- Pour la pratique, une fois le projet dézippé, mettez le dans git
- Vous devez l’importer dans Eclipse à l’aide de ‘Import Maven Project’
- Faites plusieurs commits, 1 par fonctionnalité arrangée.  Pas besoin de faire des commits séparés pour les tests.
- Une fois votre examen terminé, assurez-vous que vous avez bien fait un commit de tous les fichiers nécessaires.  (Vous pouvez vous assurer de ça en dézippant dans un autre répertoire et vérifier à vue d’oeil)
- Remettre dans Léa sous format zip.


NOTES
    - Si vous avez une incertitude, faites une supposition et documentez-la.
    - Toute documentation permise au pratique seulement.
    - Accès à internet autorisé au pratique seulement.
    - Communication interdite en tout temps.
    

PARTIE THÉORIQUE (50 pts)
-------------------------

Répondre aux questions 1 à 5 à même le fichier ou se trouve la question dans le répertoire theorie/. 


PARTIE PRATIQUE (50 pts)
------------------------

Suivez les directives de l'énoncé se trouvant dans pratique/enonce.txt.
